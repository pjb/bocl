#ifndef filename_h
#define filename_h
#include "kernel_types.h"

Object* pathname(Object* pathname_designator);
Object* namestring(Object* pathname);

#endif
